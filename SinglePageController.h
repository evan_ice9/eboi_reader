//
//  SinglePageController.h
//  PageView
//
//  Created by Fahim Ahmed on 10/22/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Manager.h"

@interface SinglePageController : UIViewController<UIWebViewDelegate,UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webview;
@property (strong, nonatomic) IBOutlet UIView *header;
@property (strong, nonatomic) IBOutlet UIView *footer;
@property (strong, nonatomic) IBOutlet UISlider *page_slider;
@property (strong, nonatomic) IBOutlet UISlider *font_size_slider;
@property (strong, nonatomic) IBOutlet UISlider *brightness_slider;
@property (strong, nonatomic) IBOutlet UILabel *chapter_title;


@property NSString *urlstr;
@property BOOL visible;

@property  int index;
@property int total_pages;
@property int current_page;
@property int font_value;
- (IBAction)value_change:(id)sender;
- (IBAction)font_size_change:(id)sender;
- (IBAction)brightness_change:(id)sender;

- (IBAction)finishd_pagescroll:(id)sender;

- (IBAction)font_change_finished:(id)sender;


@property (strong, nonatomic) IBOutlet UILabel *page_label;

@property (strong, nonatomic) IBOutlet UIButton *slider_btn;
@property (strong, nonatomic) IBOutlet UIButton *font_btn;

@property (strong, nonatomic) IBOutlet UIView *font_brightness_tab;
@property (strong, nonatomic) IBOutlet UIView *pageslide_tab;

@property  BOOL ready;

- (void)gotoPage:(int)pageIndex;

@end
