//
//  AppDelegate.h
//  PageView
//
//  Created by Fahim Ahmed on 10/22/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "TabViewController.h"
#import "InitSlidingViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
