//
//  ViewController.h
//  PageView
//
//  Created by Fahim Ahmed on 10/22/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SinglePageController.h"
#import "ZipArchive.h"
#import "EpubContent.h"
#import "XMLHandler.h"
#import "Manager.h"

@interface ViewController : UIViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate,XMLHandlerDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate,NSObject>
{
    XMLHandler *_xmlHandler;
	//EpubContent *_ePubContent;
	NSString *_pagesPath;
	//NSString *_rootPath;
	NSString *_strFileName;
	int _pageNumber;
    int  _currentPageIndex;
    bool visable;
    
    NSMutableString *mutableString;

}
@property UIPageViewController *_pagecontrol;
@property NSArray *viewcontrollers;

@property NSString *_filepath;
@property (nonatomic, retain)EpubContent *_ePubContent;
@property (nonatomic, retain)NSString *_rootPath;
//@property BOOL visable;
@property Manager *m;


@property SinglePageController *next_view;
@property SinglePageController *prev_view;
@property bool animation_finished;

@end
