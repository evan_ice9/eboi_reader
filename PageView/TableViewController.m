//
//  TableViewController.m
//  PageView
//
//  Created by Fahim Ahmed on 11/2/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import "TableViewController.h"

@interface TableViewController ()

@end

@implementation TableViewController

@synthesize data_source,complex_toc,m,ePub,_rootPath;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.tableView.dataSource=self;
    self.tableView.delegate=self;
    
    m=[Manager shared_Manager];
    ePub=[m get_epub_content];
    _rootPath=[m get_root_path];
    
    
    //self.tableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 10)];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    
    if ([data_source isKindOfClass:[NSArray class]])
    {
        NSLog(@"yes");
        NSArray *chapter_Data=[data_source mutableCopy];
        
        if (complex_toc)
        {
            return [chapter_Data count];
        }
        else
        {
            
            return 1;
        }
        
        
    }
    else
    {
         NSLog(@"no");
        return 1;
        
    }

    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    
    
    if ([data_source isKindOfClass:[NSArray class]])
    {
        NSArray *chapter_Data=[data_source mutableCopy];
        
        if (complex_toc)
        {
            
            if ([[chapter_Data objectAtIndex:section] valueForKey:@"navPoint"])
            {
                //NSLog(@"%@",[[chapter_Data objectAtIndex:section] valueForKey:@"navPoint"]);
                return     [[[chapter_Data objectAtIndex:section] valueForKey:@"navPoint"] count];
            }
            else
            {
                
                return 0;
            }
        }
        else
            return [chapter_Data count];
        
    }
    else
    {
        return [[data_source objectForKey:@"navPoint"] count];
        
    }

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cell_identifier=@"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier];
    if (cell ==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier];
    }
    if ([data_source isKindOfClass:[NSArray class]]) {
        NSArray *chapter_data=[data_source mutableCopy];
        if (complex_toc)
        {
            NSArray *Chapters_at_Section=[[chapter_data objectAtIndex:indexPath.section] valueForKey:@"navPoint"];
            cell.textLabel.text=[[Chapters_at_Section objectAtIndex:indexPath.row]valueForKeyPath:@"navLabel.text"];
        }
        else
        {
            [cell.textLabel setText:[[chapter_data objectAtIndex:indexPath.row] valueForKeyPath:@"navLabel.text"]];
            
        }
        
    }
    else
    {
        NSArray *Chapters_at_Section=[data_source valueForKey:@"navPoint"];
        cell.textLabel.text=[[Chapters_at_Section objectAtIndex:indexPath.row]valueForKeyPath:@"navLabel.text"];
        
    }
    
    [cell.textLabel setFont:[UIFont systemFontOfSize:20.0]];
    
    
    
    
    return cell;

}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *file_path=@"";
    
    if ([data_source isKindOfClass:[NSArray class]])
    {
        NSArray *Chapter_Data=[data_source mutableCopy];
        
        if (complex_toc)
        {
            
            
            NSArray *Inside_Chapter_Data=[Chapter_Data objectAtIndex:indexPath.section];
            
            file_path=[[[Inside_Chapter_Data valueForKey:@"navPoint"] objectAtIndex:indexPath.row] valueForKeyPath:@"content._src"];
            
        }
        else
        {
            file_path=[[Chapter_Data objectAtIndex:indexPath.row] valueForKeyPath:@"content._src"];
        }
        
    }
    else
    {
        
        file_path=[[[data_source objectForKey:@"navPoint"] objectAtIndex:indexPath.row]valueForKeyPath:@"content._src"];
        
    }
    
    
    @try {
        
        NSLog(@"%@",file_path);
        
        NSString *file_name=[[ePub._manifest allKeysForObject:file_path] objectAtIndex:0];
        
        NSLog(@"%d",[ePub._spine indexOfObject:file_name]);
        
        int index=[ePub._spine indexOfObject:file_name];
        
        NSDictionary *info=[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:index] forKey:@"page_index"];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:Change_page object:nil userInfo:info];
        [[NSNotificationCenter defaultCenter]postNotificationName:left_view_close object:nil];
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception.reason);
    }

}


/*
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    if ([data_source isKindOfClass:[NSArray class]])
    {
        NSArray* chapter_data=[data_source mutableCopy];
        return [[chapter_data  objectAtIndex:section] valueForKeyPath:@"navLabel.text"];
    }
    else
    {
        return [data_source valueForKeyPath:@"navLabel.text"];
        
    }
}*/

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *title=@"";
    if ([data_source isKindOfClass:[NSArray class]])
    {
        NSArray* chapter_data=[data_source mutableCopy];
        title = [[chapter_data  objectAtIndex:section] valueForKeyPath:@"navLabel.text"];
    }
    else
    {
        title = [data_source valueForKeyPath:@"navLabel.text"];
        
    }

   UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handle_tap:)];
    tap.delegate=self;

    UIView *headerview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    [headerview addGestureRecognizer:tap];
    UILabel *header_title=[[UILabel alloc]initWithFrame:CGRectMake(15, 10, 320, 20)];
    [header_title setText:title];
    [header_title setFont:[UIFont systemFontOfSize:30]];
    [header_title sizeToFit];
    
    [headerview addSubview:header_title];
    
    [headerview setBackgroundColor:[UIColor whiteColor]];
    
    headerview.tag=section;
    
    
    return headerview;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{


    return 40.0f;
}


#pragma mark tapGesture delegates

-(void)handle_tap:(UIGestureRecognizer *)gesture
{
    
    NSString *title=@"";
    if ([data_source isKindOfClass:[NSArray class]])
    {
        NSArray* chapter_data=[data_source mutableCopy];
        title = [[chapter_data  objectAtIndex:gesture.view.tag] valueForKeyPath:@"content._src"];
    }
    else
    {
        title = [data_source valueForKeyPath:@"content._src"];
        
    }
    
    
    @try {
        
        NSString *file_name=[[ePub._manifest allKeysForObject:title] objectAtIndex:0];
        
        NSLog(@"%d",[ePub._spine indexOfObject:file_name]);
        
        int index=[ePub._spine indexOfObject:file_name];
        
        NSDictionary *info=[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:index] forKey:@"page_index"];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:Change_page object:nil userInfo:info];
        [[NSNotificationCenter defaultCenter]postNotificationName:left_view_close object:nil];
        
        //NSLog(@"%@",ePub._spine);
        
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception.reason);
    }
    
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


/*
 
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
