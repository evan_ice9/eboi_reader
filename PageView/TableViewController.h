//
//  TableViewController.h
//  PageView
//
//  Created by Fahim Ahmed on 11/2/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMLDictionary.h"
#import "Manager.h"
#import "EpubContent.h"

@interface TableViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>

@property (strong,nonatomic) NSDictionary *data_source;
@property bool complex_toc;
@property Manager *m;
@property EpubContent *ePub;
@property NSString *_rootPath;
@end
