//
//  ViewController.m
//  PageView
//
//  Created by Fahim Ahmed on 10/22/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize _pagecontrol,_filepath,viewcontrollers,m;

@synthesize next_view,prev_view,animation_finished;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _filepath=@"ghaatter kthaa - Rabindranath Tagore";
    visable=NO;
    m=[Manager shared_Manager];
    
    animation_finished=YES;
    
    [m set_book_name:_filepath];
    
    [self UnzipEpub];
    _xmlHandler =[[XMLHandler alloc]init];
    _xmlHandler.delegate=self;
    [_xmlHandler parseXMLFileAt:[self getRootFilePath]];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loadPage:) name:Change_page object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(toggle_visible:) name:visibility_change object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reload_pages) name:page_adjusting object:nil];
}

#pragma mark tap_gesture_delegates

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    NSLog(@"touch");
    int max_y=isIpad?922:466;
    
    if (visable)
    {
        NSLog(@"visible");
        if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]])
        {
            NSLog(@"tap gesture");
        }
        else
        {
            
            NSLog(@"swipe gesture");
        }
        
        
        CGPoint point=[touch locationInView:self.view];
        
        
        if (point.y>=max_y)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else
    {
    
            NSLog(@"not visible");
    }


    return YES;

}


-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    
    if (!animation_finished)
    {
        return NO;
    }
    
    if (next_view)
    {
        if (!next_view.ready || !prev_view.ready)
        {
            
            return NO;
            
        }
    }
    
    SinglePageController *temp=[_pagecontrol.viewControllers objectAtIndex:0];
    
    _currentPageIndex=temp.index;
    _pageNumber=temp.current_page;
    
    //Manager *m=[Manager shared_Manager];
    
    EpubContent *epub=[m get_epub_content];

    
   // NSLog(@"should begin gesture %d %d %d",_currentPageIndex,_pageNumber,epub._spine.count);
    
    if (_currentPageIndex==0 && _pageNumber==0)
    {
        if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer  class]] && [(UIPanGestureRecognizer *)gestureRecognizer velocityInView:gestureRecognizer.view].x>0.0f )
        {
            NSLog(@"swipe disable on first page");
            return NO;
            
        }
    }
    else if (_currentPageIndex==epub._spine.count-1 && _pageNumber==temp.total_pages-1)
    {
        if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer  class]] && [(UIPanGestureRecognizer *)gestureRecognizer velocityInView:gestureRecognizer.view].x<0.0f )
        {
            NSLog(@"swipe disable on last page");
            return NO;
            
        }
    
    
    }
    
        return YES;
}



#pragma mark pageviewcontroller_delegates

-(SinglePageController *)ViewControllerAtIndex:(NSInteger)index atPage:(NSInteger)page_index
{
    
    SinglePageController *view_control=[self.storyboard instantiateViewControllerWithIdentifier:@"Single_View"];
    
    _pagesPath=[NSString stringWithFormat:@"%@/%@",self._rootPath,[self._ePubContent._manifest valueForKey:[self._ePubContent._spine objectAtIndex:index]]];
    
    NSLog(@"%@",_pagesPath);
    
	//_pageNumber=0;
    

    
    view_control.index=(int)index;
    view_control.current_page=(int)page_index;
    view_control.urlstr=_pagesPath;
    
    return view_control;

}

-(NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{


    return [self _ePubContent]._spine.count;
}


-(NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{


    return 0;
    
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
     NSLog(@"before");
    
    NSUInteger index=((SinglePageController *)viewController).index;
    NSUInteger max_page=((SinglePageController *)viewController).total_pages;
    NSUInteger current_page=((SinglePageController *)viewController).current_page;
    
    SinglePageController *temp=prev_view;
    
    return temp;
    
}


- (UIViewController *)pageViewController:
(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSLog(@"after");
    NSUInteger index=((SinglePageController *)viewController).index;
    NSUInteger max_page=((SinglePageController *)viewController).total_pages;
    NSUInteger current_page=((SinglePageController *)viewController).current_page;
    
    if (next_view) {
        
        
        SinglePageController *temp=next_view;
       
        return temp;
    }
    else
    {
    
        
        if (current_page==max_page-2) {
            if (index<self._ePubContent._spine.count-1)
            {   NSLog(@"going next index");
                index++;
                next_view=[self ViewControllerAtIndex:index atPage:0];
                [next_view.view layoutSubviews];
                
                return [self ViewControllerAtIndex:index-1 atPage:(int)current_page+1];
                }
            else
            {
                return nil;
            }
        }
        else if (current_page==max_page-1)
        {
            
            if (index<self._ePubContent._spine.count-1)
            {
                NSLog(@"going next index");
                
                index++;
                next_view=[self ViewControllerAtIndex:index atPage:1];
                [next_view.view layoutSubviews];
                
                return [self ViewControllerAtIndex:index atPage:0];
                
            }
            else
            {
                
                return nil;
                
            }
        }
        else
        {
            NSLog(@"next page");
            
            next_view=[self ViewControllerAtIndex:index atPage:(int)current_page+2];
            [next_view.view layoutSubviews];
            
            return [self ViewControllerAtIndex:index atPage:(int)current_page+1];
        }
    
    }
    
    
    
}

-(void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers
{

    //[_pagecontrol.view setUserInteractionEnabled:NO];
    animation_finished=NO;
}

-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    NSMutableDictionary *data=[[NSMutableDictionary alloc]init];
    [data setObject:(SinglePageController *)[pageViewController.viewControllers objectAtIndex:0] forKey:@"current_view"];
    [self performSelectorOnMainThread:@selector(adjust_views:) withObject:data waitUntilDone:NO];

    if (completed)
    {
     [self performSelector:@selector(enableUserInteraction) withObject:nil afterDelay:2];
    }
    else if (finished)
    {
       
     [self performSelector:@selector(enableUserInteraction) withObject:nil afterDelay:.1];
    }

}

-(void)enableUserInteraction{
    
    animation_finished=YES;
    //[_pagecontrol.view setUserInteractionEnabled:YES];
    
}


-(void)reload_pages
{

    NSMutableDictionary *data=[[NSMutableDictionary alloc]init];
    [data setObject:[_pagecontrol.viewControllers objectAtIndex:0] forKey:@"current_view"];
    [self adjust_views:data];

}

-(void)adjust_views:(NSDictionary *)data
{

   SinglePageController *current_view=[data objectForKey:@"current_view"];
   int current_page=current_view.current_page;
   int max_page =current_view.total_pages;
   int index =current_view.index;
   int font_value=[[[NSUserDefaults standardUserDefaults] objectForKey:user_font_value] floatValue];
   
   NSLog(@"%d",current_view.current_page);
    
    
    
   if (current_page==max_page-1)
    {
        
        if (index<self._ePubContent._spine.count-1)
        {
            NSLog(@"going next index");
            
            //index++;
            next_view=[self ViewControllerAtIndex:index+1 atPage:0];
            
        }
        
    }
    else
    {
        NSLog(@"next page");
        
        if (next_view.index==index && next_view.current_page==current_page+1 && font_value==next_view.font_value)
        {
            NSLog(@"next page already exists");
        }
        else
        {
            next_view=[self ViewControllerAtIndex:index atPage:(int)current_page+1];
        }
    }
    
    
    if (current_page==0) {
        
        if (index==0 || index==NSNotFound)
        {
            NSLog(@"zero...");
        }
        else
        {
            //index--;
            prev_view=[self ViewControllerAtIndex:index-1 atPage:-1];
        }
    }
    else
    {
        if (prev_view) {
            
            if (prev_view.index==index && prev_view.current_page==current_page-1 && font_value==prev_view.font_value)
            {
                NSLog(@"previous page already exists");
            }
            else
            {
                prev_view=[self ViewControllerAtIndex:index atPage:current_page-1];
                
            }
        }
        else
        {
        
           prev_view=[self ViewControllerAtIndex:index atPage:current_page-1];
        
        }
        
   
    }

    [next_view.view layoutSubviews];
    [prev_view.view layoutSubviews];
    
    NSLog(@"%d",index);
    NSLog(@"%d %d %d",prev_view.current_page,current_page,next_view.current_page);

}

#pragma mark Epub_contents

-(void)UnzipEpub
{

    ZipArchive *archiver=[[ZipArchive alloc]init];

    if ([archiver UnzipOpenFile:[[NSBundle mainBundle] pathForResource:_filepath ofType:@"epub"]]) {

        NSString *path=[NSString stringWithFormat:@"%@/Ebooks",[self applicationDocumentsDirectory]];
        NSFileManager *_filemanager=[[NSFileManager alloc]init];
        
        if ([_filemanager fileExistsAtPath:path])
        {
            NSError *err;
            [_filemanager removeItemAtPath:path error:&err];
            
        }
        
        BOOL ret=[archiver UnzipFileTo:[NSString stringWithFormat:@"%@/",path] overWrite:YES];
        
        if (ret==NO) {
            
            NSLog(@"error occured");
        }
        
        [archiver UnzipCloseFile];
        
    }


}

- (NSString *)applicationDocumentsDirectory {
	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}



#pragma mark XMLHandler Delegate Methods

- (void)foundRootPath:(NSString*)rootPath{
	
	//Found the path of *.opf file
	
	//get the full path of opf file
	NSString *strOpfFilePath=[NSString stringWithFormat:@"%@/Ebooks/%@",[self applicationDocumentsDirectory],rootPath];
	NSFileManager *filemanager=[[NSFileManager alloc] init];
	
	self._rootPath=[strOpfFilePath stringByReplacingOccurrencesOfString:[strOpfFilePath lastPathComponent] withString:@""];
    Manager *m=[Manager shared_Manager];
    [m set_root_path:__rootPath];
	
	if ([filemanager fileExistsAtPath:strOpfFilePath]) {
		
		//Now start parse this file
		[_xmlHandler parseXMLFileAt:strOpfFilePath];
	}
	else {
		
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error"
													  message:@"OPF File not found"
													 delegate:self
											cancelButtonTitle:@"OK"
											otherButtonTitles:nil];
		[alert show];
		alert=nil;
	}
	filemanager=nil;
	
}

- (NSString*)getRootFilePath{
	
	//check whether root file path exists
	NSFileManager *filemanager=[[NSFileManager alloc] init];
	NSString *strFilePath=[NSString stringWithFormat:@"%@/Ebooks/META-INF/container.xml",[self applicationDocumentsDirectory]];
	if ([filemanager fileExistsAtPath:strFilePath]) {
		
		//valid ePub
		NSLog(@"Parse now %@",[m get_book_name]);
		filemanager=nil;
		
		return strFilePath;
	}
	else {
		
		//Invalid ePub file
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error"
													  message:@"Root File not Valid"
													 delegate:self
											cancelButtonTitle:@"OK"
											otherButtonTitles:nil];
		[alert show];
		alert=nil;
		
	}
	filemanager=nil;
	return @"";
}


-(void)finishedParsing:(EpubContent *)ePubContents{
    
    self._ePubContent=ePubContents;
    [m set_epub:ePubContents];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        _pagecontrol=[[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        [_pagecontrol.view setFrame:self.view.bounds];
        _pagecontrol.dataSource=self;
        _pagecontrol.delegate=self;
        
        
        SinglePageController *top_controller=[self ViewControllerAtIndex:0 atPage:0];
        viewcontrollers=@[top_controller];
        
        [_pagecontrol setViewControllers:viewcontrollers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        [_pagecontrol.view setFrame:self.view.bounds];
        
        for (UIGestureRecognizer *tap in _pagecontrol.gestureRecognizers)
        {
            if ([tap isKindOfClass:[UITapGestureRecognizer class]])
            {
                tap.enabled=NO;
            }
            else
            {
                tap.delegate=self;
            }
        }

        
        [self addChildViewController:_pagecontrol];
        [self.view addSubview:_pagecontrol.view];
        [_pagecontrol didMoveToParentViewController:self];
    });
    
}




/*Function Name : loadPage
 *Return Type   : void
 *Parameters    : nil
 *Purpose       : To load actual pages to webview
 */

- (void)loadPage:(NSNotification *)notification
{
	
    
    NSLog(@"%@",[notification userInfo]);
    
    int index=[[[notification userInfo] objectForKey:@"page_index"] intValue];
    
    SinglePageController *top_controller;
    
    if ([[notification userInfo]objectForKey:@"page_number"])
    {
        top_controller=[self ViewControllerAtIndex:index atPage:[[[notification userInfo]objectForKey:@"page_number"] intValue]];
        [top_controller.view layoutSubviews];
    }
    else
    {
        top_controller=[self ViewControllerAtIndex:index atPage:0];
    }
    
    visable=NO;
    viewcontrollers=@[top_controller];
    [_pagecontrol setViewControllers:viewcontrollers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    NSMutableDictionary *data=[[NSMutableDictionary alloc]init];
    [data setObject:top_controller forKey:@"current_view"];
    [self adjust_views:data];
}



-(void)toggle_visible:(NSNotification *)notification
{
    BOOL value=[[[notification userInfo]objectForKey:@"visibility"] boolValue];

    NSLog(@"%d",value);
    visable=value;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
