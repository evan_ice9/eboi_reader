//
//  BookmarkViewController.m
//  PageView
//
//  Created by Fahim Ahmed on 11/6/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import "BookmarkViewController.h"

@interface BookmarkViewController ()

@end

@implementation BookmarkViewController

@synthesize datasource,epub,bookmark_data;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"bookmarkview");
    
    Manager *m=[Manager shared_Manager];
    epub=[m get_epub_content];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reload_table) name:reload_bookmark_table object:nil];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)reload_table
{
    
    Manager *m=[Manager shared_Manager];
    self.datasource=[m getBookmarks];
    [self.tableView reloadData];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return datasource.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    int page_index=[[[datasource objectAtIndex:indexPath.row] objectForKey:bookmark_index] intValue];
    int page_no=[[[datasource objectAtIndex:indexPath.row] objectForKey:bookmark_page] intValue];
    
    NSString *title;
    
    if ([bookmark_data isKindOfClass:[NSArray class]])
    {
        NSArray* chapter_data=[bookmark_data mutableCopy];
        title = [[chapter_data  objectAtIndex:page_index-1] valueForKeyPath:@"navLabel.text"];
    }
    else
    {
        title = [bookmark_data valueForKeyPath:@"navLabel.text"];
        
    }

    
    
    cell.textLabel.text=title;//[NSString stringWithFormat:@"%@",[epub._spine objectAtIndex:page_index]];
    cell.detailTextLabel.text=[NSString stringWithFormat:@"%d",page_no+1];
    
    // Configure the cell...
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int page_index=[[[datasource objectAtIndex:indexPath.row] objectForKey:bookmark_index] intValue];
    int page_no=[[[datasource objectAtIndex:indexPath.row] objectForKey:bookmark_page] intValue];

   
   [[NSUserDefaults standardUserDefaults] setObject:[[datasource objectAtIndex:indexPath.row] objectForKey:bookmark_font] forKey:user_font_size];
    
    NSMutableDictionary *info=[[NSMutableDictionary alloc]init];

    [info setObject:[NSNumber numberWithInt:page_index] forKey:@"page_index"];
    [info setObject:[NSNumber numberWithInt:page_no] forKey:@"page_number"];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:Change_page object:nil userInfo:info];
    [[NSNotificationCenter defaultCenter]postNotificationName:left_view_close object:nil];


}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
