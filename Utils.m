//
//  Utils.m
//  PageView
//
//  Created by Fahim Ahmed on 10/29/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import "Utils.h"

@implementation Utils




+ (CAGradientLayer*) yellowGradient {
    
    UIColor *colorOne = [UIColor colorWithRed:(249/255.0) green:(181/255.0) blue:(44/255.0) alpha:1.0];
    UIColor *colorTwo = [UIColor colorWithRed:(251/255.0)  green:(215/255.0)  blue:(51/255.0)  alpha:1.0];
    
    NSArray *colors = [NSArray arrayWithObjects:(id)colorTwo.CGColor, colorOne.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
    
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    
    return headerLayer;
    
}


+(UIBezierPath *)draw_triangle_startpoint:(CGPoint)start endpoint:(CGPoint)end
{
    CGPoint startPoint =    start, endPoint = end;
    
    CGFloat angle = -M_PI/6; // 60 degrees in radians
    // v1 = vector from startPoint to endPoint:
    CGPoint v1 = CGPointMake(endPoint.x - startPoint.x, endPoint.y - startPoint.y);
    // v2 = v1 rotated by 60 degrees:
    CGPoint v2 = CGPointMake(cosf(angle) * v1.x - sinf(angle) * v1.y,
                             sinf(angle) * v1.x + cosf(angle) * v1.y);
    // thirdPoint = startPoint + v2:
    CGPoint thirdPoint = CGPointMake(startPoint.x + v1.x/2, startPoint.y + v2.y);
    
    UIBezierPath *triangle = [UIBezierPath bezierPath];
    [triangle moveToPoint:startPoint];
    [triangle addLineToPoint:endPoint];
    [triangle addLineToPoint:thirdPoint];
    [triangle closePath];
    
    
    return triangle;

}

+ (UIImage *)imageNamed:(NSString *)name imagetintColor:(UIColor *)color {
    UIImage *img = nil;
    img = [UIImage imageNamed:name];
    
    UIGraphicsBeginImageContextWithOptions(img.size, NO, 0.0f);
    [color setFill];
    CGRect bounds = CGRectMake(0, 0, img.size.width, img.size.height);
    UIRectFill(bounds);
    [img drawInRect:bounds blendMode:kCGBlendModeDestinationIn alpha:1.0];
    
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return tintedImage;
    
}





@end
