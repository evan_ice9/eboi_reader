//
//  InitSlidingViewController.h
//  PageView
//
//  Created by Fahim Ahmed on 11/2/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"

@interface InitSlidingViewController : ECSlidingViewController

@end
