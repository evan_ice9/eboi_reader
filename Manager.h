//
//  Manager.h
//  PageView
//
//  Created by Fahim Ahmed on 11/3/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EpubContent.h"

@interface Manager : NSObject{

    NSString *root_path;
    EpubContent *epub;
    NSString *book_name;
    NSDictionary *chapter_list;
    
}

+(id)shared_Manager;

-(void)set_root_path:(NSString *)root;
-(NSString *)get_root_path;

-(EpubContent *)get_epub_content;
-(void)set_epub:(EpubContent *)epub_main;
-(void)setBookmarkAtIndex:(int)index page:(int)page;
-(NSArray *)getBookmarks;
-(BOOL)bookmarkExists:(int)index page:(int)page;
-(void)set_book_name:(NSString *)book;
-(NSString *)get_book_name;
-(void)set_chapter_list:(NSDictionary *)list;
-(NSDictionary *)get_chapterlist;


@end
