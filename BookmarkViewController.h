//
//  BookmarkViewController.h
//  PageView
//
//  Created by Fahim Ahmed on 11/6/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Manager.h"

@interface BookmarkViewController : UITableViewController
@property NSArray *datasource;
@property EpubContent *epub;
@property NSDictionary *bookmark_data;
@end
