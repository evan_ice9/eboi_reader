//
//  TabViewController.m
//  PageView
//
//  Created by Fahim Ahmed on 11/2/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import "TabViewController.h"

@interface TabViewController ()

@end

@implementation TabViewController

@synthesize header,tab,tabbarcontroller,chapter_btn,bookmark_tab_btn,shape;

@synthesize chapters_list,complex_toc;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadContentModel];
    
    CAGradientLayer *yellow_layer=[Utils yellowGradient];
    yellow_layer.frame=header.bounds;
    [header.layer addSublayer:yellow_layer];
    header.layer.shadowOffset=CGSizeMake(0, 1);
    header.layer.shadowRadius=1;
    header.layer.shadowOpacity=.4;
    header.layer.masksToBounds=NO;
    
    header.layer.shadowPath = [UIBezierPath bezierPathWithRect:header.bounds].CGPath;
    
    
    CALayer *border = [CALayer layer];
    border.backgroundColor = [UIColor colorWithRed:(204/255.0) green:(149/255.0) blue:(36/255.0) alpha:1.0].CGColor;
    
    border.frame = CGRectMake(0, header.frame.size.height - 1, header.frame.size.width, 1);
    [header.layer addSublayer:border];
    
    int ipad_offset=isIpad?420:0;

    
    UIImageView *logo_view=[[UIImageView alloc]initWithFrame:CGRectMake(40, 20, 74, 43)];
    [logo_view setImage:[UIImage imageNamed:@"logo"]];
    [logo_view setContentMode:UIViewContentModeScaleAspectFit];
    [header addSubview:logo_view];
    
    //UIButton *back_btn=[[UIButton alloc]initWithFrame:CGRectMake(5, 35, 9.1, 14.9)];
    UIButton *back_btn=[[UIButton alloc]initWithFrame:CGRectMake(5, 24.55, 30, 30)];
    back_btn.imageEdgeInsets=UIEdgeInsetsMake(7.55, 10.45, 7.55, 10.45);
    [back_btn setImage:[UIImage imageNamed:@"back_btn"] forState:UIControlStateNormal];
    [back_btn setContentMode:UIViewContentModeScaleAspectFit];
    [back_btn addTarget:self action:@selector(back_btn_click:) forControlEvents:UIControlEventTouchUpInside];
    [header addSubview:back_btn];
    
    
    UIButton *search_btn=[[UIButton alloc]initWithFrame:CGRectMake(265+ipad_offset, 20, 44, 44)];
    //CGRectMake(285, 35, 12.5, 13)
    [search_btn setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    search_btn.imageEdgeInsets=UIEdgeInsetsMake(15.5, 15.75, 15.5, 15.75);
    [search_btn setContentMode:UIViewContentModeScaleAspectFill];
    [header addSubview:search_btn];
    
    UIButton *bookmark_btn=[[UIButton alloc]initWithFrame:CGRectMake(225+ipad_offset, 20.1, 44, 44)];
    //CGRectMake(245, 35, 7.7, 14.2)
    [bookmark_btn setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
    bookmark_btn.imageEdgeInsets=UIEdgeInsetsMake(14.9, 18.2,14.9,18.2);
    [bookmark_btn setContentMode:UIViewContentModeScaleAspectFill];
    [header addSubview:bookmark_btn];
    
    UIButton *chapters=[[UIButton alloc]initWithFrame:CGRectMake(180+ipad_offset, 19.5, 44, 44)];
    [chapters setImage:[UIImage imageNamed:@"chapters"] forState:UIControlStateNormal];
    [chapters setContentMode:UIViewContentModeScaleAspectFill];
    chapters.imageEdgeInsets=UIEdgeInsetsMake(16, 15.5, 16, 15.5);
    [chapters addTarget:self action:@selector(btn_click:) forControlEvents:UIControlEventTouchUpInside];
    [header addSubview:chapters];
    
    TableViewController *tc1=(TableViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"tableview"];
    tc1.data_source=chapters_list;
    tc1.complex_toc=complex_toc;
    tc1.view.frame=tab.bounds;
    
    BookmarkViewController *tc2=(BookmarkViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"bookmarktable"];
    tc2.view.frame=tab.bounds;
    Manager *m=[Manager shared_Manager];
    tc2.datasource=[m getBookmarks];
    tc2.bookmark_data=chapters_list;
    
    
    NSArray *viewcontrollers=[[NSArray alloc]initWithObjects:tc1,tc2,nil];
    tabbarcontroller=[[MGSwipeTabBarController alloc]initWithViewControllers:viewcontrollers];
    
    tabbarcontroller.view.frame=tab.bounds;
    [tab addSubview:tabbarcontroller.view];
    
    chapter_btn.selected=YES;
    [self addTriangleToButton:chapter_btn];

    // Do any additional setup after loading the view.
}

-(void)viewdidAppear:(BOOL)animated
{
}


-(IBAction)btn_click:(id)sender
{

    [[NSNotificationCenter defaultCenter]postNotificationName:left_view_close object:nil];

}

-(IBAction)back_btn_click:(id)sender
{

    [[NSNotificationCenter defaultCenter]postNotificationName:left_view_close object:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)tabbar_btn_onclick:(id)sender
{
    
    NSLog(@"%d",[sender tag]);
    UIButton *btn=(UIButton *)sender;
    if (btn.selected)
    {
        return;
    }
    else
    {
    
        switch ([sender tag])
        {
        case 10:
        {
           [tabbarcontroller setSelectedIndex:0];
            chapter_btn.selected=YES;
            bookmark_tab_btn.selected=NO;
            [shape removeFromSuperlayer];
            
            [self addTriangleToButton:btn];
            
            break;
        }
        
        case 20:
        {
            
            [tabbarcontroller setSelectedIndex:1];
            chapter_btn.selected=NO;
            bookmark_tab_btn.selected=YES;
            [shape removeFromSuperlayer];
            
           
            [self addTriangleToButton:btn];
            
            break;
        }
            
        default:
            break;
            
        }
    }

}



-(void)addTriangleToButton:(UIButton *)btn
{


    float y = btn.frame.size.height;
    float x = btn.frame.size.width/2;
    
    CGPoint start=CGPointMake(x-5, y);
    CGPoint end=CGPointMake(x+5, y);
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [[Utils draw_triangle_startpoint:start endpoint:end]CGPath];
    shapeLayer.fillColor = [UIColor whiteColor].CGColor;
    shapeLayer.strokeColor = [UIColor whiteColor].CGColor;
    shape=shapeLayer;
    [btn.layer addSublayer:shapeLayer];

}

-(void)loadContentModel
{
    Manager *m=[Manager shared_Manager];
    EpubContent *data_source=[m get_epub_content];
    NSString *root_path=[m get_root_path];
    
    
    NSString *Toc=@"";
    for (NSString* str  in [data_source._manifest allValues]) {
        if ([str rangeOfString:@"ncx"].location!=NSNotFound) {
            NSLog(@"%@",str);
            Toc=str;
        }
    }
    NSString* filePath = [NSString stringWithFormat:@"%@/%@",root_path,Toc];;
    NSLog(@"%@",filePath);
    /*NSData *fileData1 = [NSData dataWithContentsOfFile:filePath];
     NSString *urlString = [[NSString alloc] initWithData:fileData1 encoding:NSASCIIStringEncoding];
     NSData* data=[urlString dataUsingEncoding:NSUTF8StringEncoding];*/
    
    
    NSData *data=[[NSData alloc]initWithContentsOfURL:[NSURL fileURLWithPath:filePath]];
    
    XMLDictionaryParser *parser=[[XMLDictionaryParser alloc]init];
    
    NSDictionary *dict=[parser dictionaryWithData:data];
    
    
    chapters_list=[dict valueForKeyPath:@"navMap.navPoint"];
    [m set_chapter_list:chapters_list];
    
    //NSLog(@"%@",chapters);
    
    complex_toc=NO;
    
    if ([chapters_list isKindOfClass:[NSArray class]])
    {
        NSArray *chapter_Data=[chapters_list mutableCopy];
        
        for (NSDictionary *dictionary in chapter_Data)
        {
            if ([dictionary objectForKey:@"navPoint"])
            {
                complex_toc=YES;
                break;
            }
            
            NSLog(@"%d",complex_toc);
            
        }
    }




}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
