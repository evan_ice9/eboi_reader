//
//  SinglePageController.m
//  PageView
//
//  Created by Fahim Ahmed on 10/22/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import "SinglePageController.h"

@interface SinglePageController ()

@end

@implementation SinglePageController

@synthesize index,webview,urlstr,total_pages,current_page,header,visible,footer;


@synthesize page_slider,page_label,slider_btn,font_btn,chapter_title;


@synthesize pageslide_tab,font_brightness_tab,font_size_slider,brightness_slider;

@synthesize font_value,ready;


- (void)viewDidLoad
{
    [super viewDidLoad];
   
    webview.delegate=self;
    webview.paginationMode = UIWebPaginationBreakingModeColumn;
    visible=NO;
    slider_btn.selected=YES;
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    //slider controll
    brightness_slider.maximumValue=4;
    brightness_slider.minimumValue=1;
    
    font_size_slider.maximumValue=4;
    font_size_slider.minimumValue=1;
    
    Manager *m=[Manager shared_Manager];
    
    NSDictionary *chapter_list=[m get_chapterlist];
    
    
    if (index>0) {
        
        if ([chapter_list isKindOfClass:[NSArray class]])
        {
            NSArray* chapter_data=[chapter_list mutableCopy];
            chapter_title.text = [[chapter_data  objectAtIndex:index-1] valueForKeyPath:@"navLabel.text"];
        }
        else
        {
            chapter_title.text = [chapter_list valueForKeyPath:@"navLabel.text"];
            
        }
    }
   

    
    
    
    
    //gradient and header
    CAGradientLayer *yellow_layer=[Utils yellowGradient];
    yellow_layer.frame=header.bounds;
    [header.layer addSublayer:yellow_layer];
    header.layer.shadowOffset=CGSizeMake(0, 1);
    header.layer.shadowRadius=1;
    header.layer.shadowOpacity=.4;
    header.layer.masksToBounds=NO;
    
    header.layer.shadowPath = [UIBezierPath bezierPathWithRect:header.bounds].CGPath;
    
    
    CALayer *border = [CALayer layer];
    border.backgroundColor = [UIColor colorWithRed:(204/255.0) green:(149/255.0) blue:(36/255.0) alpha:1.0].CGColor;
    
    border.frame = CGRectMake(0, header.frame.size.height - 1, header.frame.size.width, 1);
    [header.layer addSublayer:border];
    
    
    int ipad_offset=isIpad?420:0;
    UIImageView *logo_view=[[UIImageView alloc]initWithFrame:CGRectMake(40, 20, 74, 43)];
    [logo_view setImage:[UIImage imageNamed:@"logo"]];
    [logo_view setContentMode:UIViewContentModeScaleAspectFit];
    [header addSubview:logo_view];
    
    UIButton *back_btn=[[UIButton alloc]initWithFrame:CGRectMake(5, 24.55, 30, 30)];
    back_btn.imageEdgeInsets=UIEdgeInsetsMake(7.55, 10.45, 7.55, 10.45);
    [back_btn setImage:[UIImage imageNamed:@"back_btn"] forState:UIControlStateNormal];
    [back_btn setContentMode:UIViewContentModeScaleAspectFit];
    [header addSubview:back_btn];
    
    UIButton *search_btn=[[UIButton alloc]initWithFrame:CGRectMake(265+ipad_offset, 20, 44, 44)];
    //CGRectMake(285, 35, 12.5, 13)
    [search_btn setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    search_btn.imageEdgeInsets=UIEdgeInsetsMake(15.5, 15.75, 15.5, 15.75);
    [search_btn setContentMode:UIViewContentModeScaleAspectFill];
    [header addSubview:search_btn];
    
    UIButton *bookmark_btn=[[UIButton alloc]initWithFrame:CGRectMake(225+ipad_offset, 20.1, 44, 44)];
    //CGRectMake(245, 35, 7.7, 14.2)
    
    [bookmark_btn setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
    [bookmark_btn setImage:[Utils imageNamed:@"bookmark" imagetintColor:ebook_red] forState:UIControlStateSelected];
    bookmark_btn.imageEdgeInsets=UIEdgeInsetsMake(14.9, 18.2,14.9,18.2);
    [bookmark_btn setContentMode:UIViewContentModeScaleAspectFill];
    [bookmark_btn addTarget:self action:@selector(bookmarkBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bookmark_btn setTag:88];
    
    [header addSubview:bookmark_btn];
    
    UIButton *chapters=[[UIButton alloc]initWithFrame:CGRectMake(180+ipad_offset, 19.5, 44, 44)];
    [chapters setImage:[UIImage imageNamed:@"chapters"] forState:UIControlStateNormal];
    [chapters setContentMode:UIViewContentModeScaleAspectFill];
    chapters.imageEdgeInsets=UIEdgeInsetsMake(16, 15.5, 16, 15.5);
    //[chapters imageRectForContentRect:CGRectMake(15.5, 16, 15, 12)];
    [chapters addTarget:self action:@selector(Chapter_btn_click:) forControlEvents:UIControlEventTouchUpInside];
    [header addSubview:chapters];
    
    
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handle_tap:)];
    tap.delegate=self;
    [self.view addGestureRecognizer:tap];


    
    
    
    if (urlstr)
    {
        NSLog(@"loading webview");
        [webview loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:urlstr]]];
    }
    
    
    
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated
{

}

-(void)viewWillAppear:(BOOL)animated
{

    [super viewWillAppear:animated];
    
    [self performSelector:@selector(scroll_to_page) withObject:nil afterDelay:.05f];

}



-(void)scroll_to_page
{
    if (current_page==-1)
    {
        [self gotoPage:total_pages-1];
        current_page=total_pages-1;
    }
    else
    {
        [self gotoPage:current_page];
    }



}
#pragma mark gesture_recognizer

-(void)handle_tap:(UITapGestureRecognizer *)tap_request
{
    
    
    
    NSLog(@"tapped %d",visible);
    
    visible=!visible;
    
   NSDictionary *info=[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:visible] forKey:@"visibility"];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:visibility_change object:nil userInfo:info];
    
    if (visible) {
        header.hidden=NO;
        footer.hidden=NO;
        [UIView animateWithDuration:0.5f animations:^{
            
            
            CGRect frame=header.frame;
            
            frame.origin.y=0;
            [header setFrame:frame];
            
            
            CGRect frame2=footer.frame;
            frame2.origin.y=isIpad?922:466;
            [footer setFrame:frame2];
            
            
        }];
    }else
    {
        
        [UIView animateWithDuration:0.5f animations:^{
            
            CGRect frame=header.frame;
            
            frame.origin.y=-65;
            [header setFrame:frame];
            
            CGRect frame2=footer.frame;
            frame2.origin.y=isIpad?1024:568;
            [footer setFrame:frame2];
            
        } completion:^(BOOL finished) {
            
            
            if (finished) {
                header.hidden=YES;
                footer.hidden=YES;
            }
        }];
        
        
    }
    
    
    
}



#pragma mark webview functions

- (void)gotoPage:(int)pageIndex
{
    NSLog(@"%d",pageIndex);
    
   font_size_slider.value=[[[NSUserDefaults standardUserDefaults] objectForKey:user_font_value] floatValue];
    
    //current_page=pageIndex;
    //_currentPageIndex = pageIndex;
    float pageOffset = pageIndex * webview.bounds.size.width;
    NSLog(@"%f",pageOffset);
    if (webview.scrollView.contentOffset.x!=pageOffset)
    {
        
            [webview.scrollView setContentOffset:CGPointMake(pageOffset, 0) animated:NO];
    }
    else
    {
    
        NSLog(@"already scrolled");
    
    }

    
    
    
    
    //[page_label setText:[NSString stringWithFormat:@"%d of %d",pageIndex+1,(int)webview.pageCount] ];
    
    //pageCountlbl.text=[NSString stringWithFormat:@"Page %d/%d",_currentPageIndex+1,_pageCount];
    
}



-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"loading webview");
    
   [webview stringByEvaluatingJavaScriptFromString:[[NSUserDefaults standardUserDefaults] objectForKey:user_font_size]];
    
    total_pages=(int)webview.pageCount;
    
    
    page_slider.maximumValue = webView.pageCount-1;
    font_size_slider.value=[[[NSUserDefaults standardUserDefaults] objectForKey:user_font_value] floatValue];
    font_value=[[[NSUserDefaults standardUserDefaults] objectForKey:user_font_value] floatValue];
    [page_slider setValue:current_page];

    [page_label setText:[NSString stringWithFormat:@"%d of %d",current_page+1,(int)webview.pageCount] ];
    
    if (current_page==-1)
    {
        [self gotoPage:total_pages-1];
        current_page=total_pages-1;
    }
    else
    {
        [self gotoPage:current_page];
    }

    
    ready=YES;
    
    Manager *m=[Manager shared_Manager];
    UIButton *bookmark_btn=(UIButton *)[self.view viewWithTag:88];
    bookmark_btn.selected=[m bookmarkExists:index page:current_page];
    

}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{

    NSLog(@"%@",error);

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)value_change:(id)sender {
    
    NSLog(@"%f",page_slider.value);
    
    [self gotoPage:(int)page_slider.value];
    current_page=(int)page_slider.value;
    [page_label setText:[NSString stringWithFormat:@"%d of %d",current_page+1,total_pages] ];
    Manager *m=[Manager shared_Manager];
    UIButton *bookmark_btn=(UIButton *)[self.view viewWithTag:88];
    bookmark_btn.selected=[m bookmarkExists:index page:current_page];
}

- (IBAction)font_size_change:(id)sender {
    
    /*NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'",
                          70+(int)font_size_slider.value*20];
    
    NSLog(@"%d",(int)font_size_slider.value*20);
    
    [webview stringByEvaluatingJavaScriptFromString:jsString];
    
    NSUserDefaults *user_info=[NSUserDefaults standardUserDefaults];
    [user_info setObject:jsString forKey:user_font_size];
    [user_info setInteger:(int)font_size_slider.value forKey:user_font_value];
    
    int    pageIndex = webview.scrollView.contentOffset.x / webview.scrollView.frame.size.width;

    current_page = pageIndex;
    
    page_slider.maximumValue = webview.pageCount-1;
    
    NSLog(@"%d",(int)webview.pageCount);
    
    //[page_label setText:[NSString stringWithFormat:@"%d of %d",pageIndex+1,(int)webview.pageCount] ];
    total_pages=(int)webview.pageCount;
    
    
    //float pageOffset = pageIndex * webview.bounds.size.width;
    //[webview.scrollView setContentOffset:CGPointMake(pageOffset, 0) animated:NO];
    
    //[page_label setText:[NSString stringWithFormat:@"%d of %d",pageIndex+1,total_pages] ];

    
    
    //[webview reload];
    
    */
    
}

- (IBAction)brightness_change:(id)sender {
    
    
    
}

- (IBAction)finishd_pagescroll:(id)sender {
    
    NSLog(@"finished pagescroll");
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        [[NSNotificationCenter defaultCenter]postNotificationName:page_adjusting object:nil];
        
        //[page_label setText:[NSString stringWithFormat:@"%d of %d",current_page,(int)webview.pageCount] ];
        
        
        
    });
}

- (IBAction)font_change_finished:(id)sender
{
    
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'",
                          70+(int)font_size_slider.value*20];
    
    NSLog(@"%d",(int)font_size_slider.value*20);
    
    [webview stringByEvaluatingJavaScriptFromString:jsString];
    
    NSUserDefaults *user_info=[NSUserDefaults standardUserDefaults];
    [user_info setObject:jsString forKey:user_font_size];
    [user_info setInteger:(int)font_size_slider.value forKey:user_font_value];
    
    int    pageIndex = webview.scrollView.contentOffset.x / webview.scrollView.frame.size.width;
    
    current_page = pageIndex;
    
    page_slider.maximumValue = webview.pageCount-1;
    
    NSLog(@"%d",(int)webview.pageCount);
    
    //[page_label setText:[NSString stringWithFormat:@"%d of %d",pageIndex+1,(int)webview.pageCount] ];
    total_pages=(int)webview.pageCount;
    
    [[NSNotificationCenter defaultCenter]postNotificationName:page_adjusting object:nil];

    
}


-(IBAction)Chapter_btn_click:(id)sender
{
    NSLog(@"chapter btn clicked");
    /*visible=!visible;
    
    NSDictionary *info=[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:visible] forKey:@"visibility"];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:visibility_change object:nil userInfo:info];*/

    

    [[NSNotificationCenter defaultCenter]postNotificationName:open_left_view object:nil];

}

-(IBAction)bookmarkBtnClick:(id)sender
{

    UIButton *btn=(UIButton *)sender;
    btn.selected=!btn.selected;
    
    
    Manager *m=[Manager shared_Manager];
    [m setBookmarkAtIndex:index page:current_page];
    
    NSLog(@"%@",[m getBookmarks]);
}

- (IBAction)onclick_btn:(id)sender {

    NSLog(@"clicked");
    
    UIButton *btn=(UIButton *)sender;
    if (btn.selected) {
        return;
    }
    
    if (btn.tag==10) {
        font_btn.selected=NO;
        slider_btn.selected=YES;
    }else if(btn.tag==20)
    {
        font_btn.selected=YES;
        slider_btn.selected=NO;
    
    }
    
    
    NSLog(@"%d %d",font_btn.selected,slider_btn.selected);
    if (font_btn.selected) {
        font_brightness_tab.hidden=NO;
        pageslide_tab.hidden=YES;
    }
    else if(slider_btn.selected)
    {
        
        font_brightness_tab.hidden=YES;
        pageslide_tab.hidden=NO;
    
    
    }
        
    
    
}


@end
