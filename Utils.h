//
//  Utils.h
//  PageView
//
//  Created by Fahim Ahmed on 10/29/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject


+(CAGradientLayer*) yellowGradient;

+(UIBezierPath *)draw_triangle_startpoint:(CGPoint)start endpoint:(CGPoint)end;

+(UIImage *)imageNamed:(NSString *)name imagetintColor:(UIColor *)color;

@end
