//
//  Manager.m
//  PageView
//
//  Created by Fahim Ahmed on 11/3/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import "Manager.h"

@implementation Manager

+(id)shared_Manager{

    static Manager *my_shared_manager=nil;
    static dispatch_once_t oncetoken;
    
    dispatch_once(&oncetoken, ^{
        
        my_shared_manager=[[self alloc] init];
        
    });


    return my_shared_manager;
}


-(void)set_root_path:(NSString *)root
{
    
    root_path=root;

}


-(NSString *)get_root_path
{


    return root_path;
}

-(EpubContent *)get_epub_content
{

    return epub;
}

-(void)set_epub:(EpubContent *)epub_main
{
    epub=epub_main;

}


-(void)set_book_name:(NSString *)book
{

    book_name=book;
    
}


-(NSString *)get_book_name
{
    return book_name;
}

-(void)setBookmarkAtIndex:(int)index page:(int)page
{

    NSUserDefaults *userdefaults=[NSUserDefaults standardUserDefaults];
    
    if ([userdefaults objectForKey:bookmark])
    {
        NSMutableDictionary *temp_dict=[[userdefaults objectForKey:bookmark] mutableCopy];

        NSMutableArray *temp_arr=[[NSMutableArray alloc]init];
        
        if ([temp_dict objectForKey:[self get_book_name]])
        {
           temp_arr=[[temp_dict objectForKey:[self get_book_name]] mutableCopy];
        }
        
        NSMutableDictionary *info=[[NSMutableDictionary alloc]init];
        
        [info setObject:[NSNumber numberWithInt:index] forKey:bookmark_index];
        [info setObject:[NSNumber numberWithInt:page] forKey:bookmark_page];
        [info setObject:[[NSUserDefaults standardUserDefaults] objectForKey:user_font_size] forKey:bookmark_font];
        
        if ([temp_arr containsObject:info])
        {
            [temp_arr removeObject:info];
        }
        else
        {
            [temp_arr addObject:info];
        }
        
        [temp_dict setObject:temp_arr forKey:[self get_book_name]];
        
        [userdefaults setObject:temp_dict forKey:bookmark];
        
        [userdefaults synchronize];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:reload_bookmark_table object:nil];
    }
    else
    {
        NSMutableDictionary *info=[[NSMutableDictionary alloc]init];
        
        [info setObject:[NSNumber numberWithInt:index] forKey:bookmark_index];
        [info setObject:[NSNumber numberWithInt:page] forKey:bookmark_page];
        [info setObject:[[NSUserDefaults standardUserDefaults] objectForKey:user_font_size] forKey:bookmark_font];
        
        NSArray *temp_arr=[NSArray arrayWithObject:info];
        
        
        NSMutableDictionary *temp_dict=[[NSMutableDictionary alloc]init];
        [temp_dict setObject:temp_arr forKey:[self get_book_name]];
        
        [userdefaults setObject:temp_dict forKey:bookmark];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:reload_bookmark_table object:nil];
        
        [userdefaults synchronize];
    
    }

}


-(NSArray *)getBookmarks
{

    NSUserDefaults *userdefaults=[NSUserDefaults standardUserDefaults];
    NSLog(@"%@",[userdefaults objectForKey:bookmark]);
    return [[userdefaults objectForKey:bookmark] objectForKey:[self get_book_name]];

}

-(void)set_chapter_list:(NSDictionary *)list
{

    chapter_list=list;

}

-(NSDictionary *)get_chapterlist
{

    return chapter_list;
}


-(BOOL)bookmarkExists:(int)index page:(int)page
{

    NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
    [dictionary setObject:[NSNumber numberWithInt:index] forKey:bookmark_index];
    [dictionary setObject:[NSNumber numberWithInt:page] forKey:bookmark_page];
    [dictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:user_font_size] forKey:bookmark_font];
    
    NSUserDefaults *userdefaults=[NSUserDefaults standardUserDefaults];
    NSMutableArray *temp_arr=[[[userdefaults objectForKey:bookmark] objectForKey:[self get_book_name]] mutableCopy];
    
    
    if ([temp_arr containsObject:dictionary])
    {
        return YES;
    }
    else
    {
        return NO;
    
    }

}




@end
