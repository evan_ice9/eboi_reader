//
//  InitSlidingViewController.m
//  PageView
//
//  Created by Fahim Ahmed on 11/2/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import "InitSlidingViewController.h"

@interface InitSlidingViewController ()

@end

@implementation InitSlidingViewController

-(void)viewDidAppear:(BOOL)animated
{

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSideBtnClick) name:open_left_view object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onSideBtnClose) name:left_view_close object:nil];

}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)onSideBtnClick
{

    [self anchorTopViewToRightAnimated:YES];

}

-(void)onSideBtnClose
{

    [self resetTopViewAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
