//
//  TabViewController.h
//  PageView
//
//  Created by Fahim Ahmed on 11/2/14.
//  Copyright (c) 2014 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewController.h"
#import "EpubContent.h"
#import "Manager.h"
#import "BookmarkViewController.h"

@interface TabViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *header;
@property (strong, nonatomic) IBOutlet UIView *tab;
@property (strong,nonatomic) MGSwipeTabBarController *tabbarcontroller;
@property (strong, nonatomic) IBOutlet UIButton *chapter_btn;
@property (strong, nonatomic) IBOutlet UIButton *bookmark_tab_btn;
@property CALayer *shape;

@property (strong,nonatomic) NSDictionary *chapters_list;

@property bool complex_toc;


@end
